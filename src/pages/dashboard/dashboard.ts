﻿import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as echarts from "echarts";
import * as config from "../../app/config/config.json";
import * as d3 from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";
import * as d3Array from "d3-array";
import * as d3Axis from "d3-axis";

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  selectedItem: any;
  icons: string[];
  items: Array<{ title: string, note: string, icon: string }>;

  //margin = {top: 20, right: 0, bottom: 20, left: 0};
  margin = {top: 0, right: 20, bottom: 0, left: 40}
  width: number;
  height: number;
  radius: number;

  arc: any;
  labelArc: any;
  labelPer: any;
  pie: any;
  color: any;
  svg: any;
  sum: number;
  x: any;
  y: any;
  g: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient) {
    this.width = 900 - this.margin.left - this.margin.right ;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.radius = Math.min(this.width, this.height) / 2;
  }

  ionViewDidEnter() {
    const apiUrl = config.apiUrl;

    // this.showPieChart('second-pie-chart');
    this.httpClient.get(`${apiUrl}/diagram/dinner`)
      .subscribe((data: any) => {
        const keys = Object.keys(data);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'first-pie-chart';
        var labelTitle = '夕食作りの週間所要時間 (D3)';
        this.calculateSum(seriesData)
        this.initSvg(selectedChart);
        this.drawPieChart(seriesData, labelTitle);
        //this.showPieChart('first-pie-chart', { legendData, seriesData, selected }, '夕食作りの週間所要時間', 'BBB');
      });
    this.httpClient.get(`${apiUrl}/diagram/dinner`)
    // this.httpClient.get(`${apiUrl}/diagram/experience`)
      .subscribe((data: any) => {
        const keys = Object.keys(data);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'second-pie-chart';
        var labelTitle = '夕食作りの週間所要時間 (Echarts)';
        this.showPieChart(selectedChart, { legendData, seriesData, selected }, labelTitle, 'BBB');
        //this.showPieChart('second-pie-chart', { legendData, seriesData, selected }, '定期宅配の利用経験', 'BBB');
      });
    this.httpClient.get(`${apiUrl}/diagram/experience`)
      .subscribe((data: any) => {
        const keys = Object.keys(data);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'third-pie-chart';
        var labelTitle = '定期宅配の利用経験 (D3)';
        this.initSvg(selectedChart);
        this.calculateSum(seriesData);
        this.drawPieChart(seriesData, labelTitle);
        //this.showPieChart('first-pie-chart', { legendData, seriesData, selected }, '夕食作りの週間所要時間', 'BBB');
      });
        //this.httpClient.get(`${apiUrl}/diagram/dinner`)
    this.httpClient.get(`${apiUrl}/diagram/experience`)
      .subscribe((data: any) => {
        const keys = Object.keys(data);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'fourth-pie-chart';
        var labelTitle = '定期宅配の利用経験 (Echarts)';
        //this.showPieChart('second-pie-chart', { legendData, seriesData, selected }, labelTitle, 'BBB');
        this.showPieChart(selectedChart, { legendData, seriesData, selected }, labelTitle, 'BBB');
      });
    this.httpClient.get(`${apiUrl}/diagram/preference`)
      .subscribe((data: any) => {
        const keys = Object.keys(data).sort((key1, key2) => data[key1].result - data[key2].result);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'first-bar-chart';
        var labelTitle = '商品の気に入った点 (D3)';
        this.initSvgForBarChart(selectedChart);
        this.initAxisForBarChart(seriesData);
        this.drawAxisForBarChart()
        this.drawBarChart(seriesData, labelTitle);
      });
    this.httpClient.get(`${apiUrl}/diagram/preference`)
      .subscribe((data: any) => {
        const keys = Object.keys(data).sort((key1, key2) => data[key1].result - data[key2].result);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => data[key].result);

        this.showBarChart('second-bar-chart', { legendData, seriesData }, '商品の気に入った点 (Echarts)', '#3398DB');
      });
    this.httpClient.get(`${apiUrl}/diagram/concern`)
      .subscribe((data: any) => {
        const keys = Object.keys(data).sort((key1, key2) => data[key1].result - data[key2].result);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => {
          return { name: data[key].label, value: data[key].result };
        });
        var selected = Array(keys.length).map(_ => true);
        var selectedChart = 'third-bar-chart';
        var labelTitle = '商品の改善点 (D3)';
        this.initSvgForBarChart(selectedChart);
        this.initAxisForBarChart(seriesData);
        this.drawAxisForBarChart()
        this.drawBarChart(seriesData, labelTitle);
      });
    this.httpClient.get(`${apiUrl}/diagram/concern`)
      .subscribe((data: any) => {
        const keys = Object.keys(data).sort((key1, key2) => data[key1].result - data[key2].result);
        var legendData = keys.map(key => data[key].label);
        var seriesData = keys.map(key => data[key].result);

        this.showBarChart('fourth-bar-chart', { legendData, seriesData }, '商品の改善点 (Echarts)', '#32988B');
      });
  }

  initSvg(selectedChart:string){
    this.color = d3Scale.scaleOrdinal()
        .range(["#FFA500", "#00FF00", "#FF0000", "#6b486b", "#FF00FF", "#d0743c", "#00FA9A"]);
    this.arc = d3Shape.arc()
        .outerRadius(this.radius - 100)
        .innerRadius(0);
    this.labelArc = d3Shape.arc()
        .outerRadius(this.radius - 80)
        .innerRadius(this.radius - 80);
    
    this.labelPer = d3Shape.arc()
        .outerRadius(this.radius - 140)
        .innerRadius(this.radius - 140);
    
    this.pie = d3Shape.pie()
        .sort(null)
        .value((d: any) => d.value);

    this.svg = d3.select("#"+ selectedChart)
        .append("svg")
        .attr("width", '100%')
        .attr("height", '100%')
        .attr('viewBox','0 0 '+Math.min(this.width,this.height)+' '+Math.min(this.width,this.height))
        .append("g")
        .attr("transform", "translate(" + Math.min(this.width,this.height) / 2 + "," + Math.min(this.width,this.height) / 2 + ")");
  }

  initSvgForBarChart(selectedChart:string){
    this.svg = d3.select("#"+ selectedChart)
        .append("svg")
        .attr("width", '100%')
        .attr("height", '100%')
        .attr('viewBox','0 0 900 500');
    this.g = this.svg.append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
  }

  initAxisForBarChart(seriesData:Object){
    this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(0.1);
    this.y = d3Scale.scaleLinear().rangeRound([this.height, 0]);
    this.x.domain(seriesData.map((d) => d.name));
    this.y.domain([0, d3Array.max(seriesData, (d) => d.value)]);
  }

  drawAxisForBarChart(){
    this.g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + this.height + ")")
        .call(d3Axis.axisBottom(this.x));
    this.g.append("g")
        .attr("class", "axis axis--y")
  
        .call(d3Axis.axisLeft(this.y))
        .append("text")
        .attr("class", "axis-title")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "1em")
        .attr("text-anchor", "end")

  }


  drawBarChart(seriesData:Object, labelTitle:string){
    let g = this.g.selectAll(".bar")
      .data(seriesData)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", (d) => this.x(d.name) )
      .attr("dx", "-1em")
      .attr("y", (d) => this.y(d.value) )
      .attr("width", this.x.bandwidth())
      .attr("height", (d) => this.height - this.y(d.value) );
     g.append("title")
      .text((d: any) => d.value);

  }

  drawPieChart(seriesData:Object, labelTitle:string) {
    let g = this.svg.selectAll(".arc")
        .data(this.pie(seriesData))
        .enter().append("g")
        .attr("class", "arc");
    g.append("path").attr("d", this.arc)
        .style("fill", (d: any) => this.color(d.data.name));
    g.append("title")
        .text((d: any) => d.data.value + "(" + this.roundUp(((d.data.value/this.sum)*100), 2) + "%)");
        
    g.append("text").attr("transform", (d: any) => "translate(" + this.labelArc.centroid(d) + ")")
        .attr("dy", ".85em")
        .attr("dx", "-1.5em")
        .text((d: any) => d.data.name);
    //g.append("text").attr("transform", (d: any) => "translate(" + this.labelPer.centroid(d) + ")")
        //.attr("dy", ".85em")
        //.attr("dx", "-1.0em")
        //.text((d: any) => d.data.value + "(" + this.roundUp(((d.data.value/this.sum)*100), 2) + "%)");
    g.append("text")
        .attr("y", 0 - (430/2))
        .attr("text-anchor", "middle")
        .style("font-size", "20px")
        .style("text-decoration", "bold")
        .text(labelTitle);
  }

  calculateSum(seriesData:Object){
    this.sum = 0;
    for(var i=0; i<seriesData.length; i++){
       this.sum = this.sum + seriesData[i].value;
    } 
  }

  roundUp(num:any, precision:any) {
    precision = Math.pow(10, precision);
    return (Math.ceil(num * precision) / precision);
  }

  showPieChart(selector: string, data: { legendData: Array<string>, seriesData: Object, selected: Array<boolean> }, title: string, seriesName: string) {
    const ec = echarts as any;

    const container = document.getElementById(selector);

    var chart = ec.init(container);
    // data = this.genData(50);
    var option = {
      title: {
        text: title,
        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: data.legendData,
        selected: data.selected
      },
      series: [
        {
          name: name,
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          data: data.seriesData,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

    chart.setOption(option);
  }

  showBarChart(selector: string, data: any, title: string, color: string) {
    const ec = echarts as any;
    console.log(data);

    const container = document.getElementById(selector);

    var chart = ec.init(container);
    var option = {
      color: color,
      title: {
        text: title,
        x: 'center'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: data.legendData,
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: '',
          type: 'bar',
          barWidth: '60%',
          data: data.seriesData
        }
      ]
    };
    chart.setOption(option);
  }
}
