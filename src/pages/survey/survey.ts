﻿import { Survey } from './../../app/entities/survey';
import { CompletionPage } from './../../pages/completion/completion';
import { AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import * as config from "../../app/config/config.json";

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html'
})
export class SurveyPage {
  survey: Survey;
  title: string = 'オーガニック商品の調査';

  constructor(public navCtrl: NavController, public httpClient: HttpClient, public alertCtrl: AlertController) {
    this.survey = new Survey(null, null, null, null);
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Do you want to submit this survey?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: () => {
            const apiUrl = config.apiUrl;
            console.log('Submitted survey to' + apiUrl);
            this.httpClient.post(apiUrl + '/survey', this.survey).subscribe((data: any) => {
              console.log(data);
              this.pushPage();
            });

          }
        }
      ]
    });
    alert.present();
  }

  pushPage() {
    this.navCtrl.push(CompletionPage);
  }

  public onSubmit() {
    if(this.survey["dinner"] == null || this.survey["preference"] == null || this.survey["concern"] == null || this.survey["experience"] == null ){
      alert("質問事項を全て選択して下さい。");
    }
    else{
      this.presentConfirm();
    }
  }

}
