import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompletionPage } from './completion';

@NgModule({
  declarations: [
    CompletionPage,
  ],
  imports: [
    IonicPageModule.forChild(CompletionPage),
  ],
})
export class CompletionPageModule {}
