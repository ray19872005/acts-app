﻿export class Survey {
  dinner: string;
  preference: string;
  concern: string;
  experience: string;

  constructor(dinner: string, preference: string, concern: string, experience: string) {
    this.dinner = dinner;
    this.preference = preference;
    this.concern = concern;
    this.experience = experience;
  }
}
