declare module '*/config.json' {
  interface Config {
    apiUrl: string
  }

  const value: Config;
  export = value;
}
